/* ---Bank Simulation-------------------------------------------
   Written by: Kanyarush Changlor   Chiang Mai University
   ID: 580611007                    Written on: Aug 24, 2016
   ------------------------------------------------------------*/
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int random(int min, int max) //range : [min, max)
{
   static bool first = true;
   if (first)
   {
      srand(time(0)); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (max - min);
}

int fastestCashier(int *cashier, int numberOfCashier)
{
    //loop to see cashier's time
    int index = 0;
    for(int i =0; i<numberOfCashier; i++)
        //cout<<cashier[i]<<"\t";
    //cout<<endl;

    if(!cashier[index])
        return index;
    for(int i=1; i< numberOfCashier; i++)
    {
        if(!cashier[i])
            return i;
        if(cashier[i] < cashier[index])
        {
            index = i;
        }
    }
    return index;
}

int main()
{

    int maxCus, minCus, maxServ, minServ, numCash, randCus, *cus, *cash, x=0, min_total, minute;

    cout    << "\n- Welcome to the Bank Simulation Program -"
            << "\n------------------------------------------";

    cout<<"\nMaxCustomers: ";
    cin>>maxCus;
    cout<<"MinCustomers: ";
    cin>>minCus;
    cout<<"MaxServiceTime: ";
    cin>>maxServ;
    cout<<"MinServiceTime: ";
    cin>>minServ;
    cout<<"NumCashiers: ";
    cin>>numCash;

    randCus = random(minCus, maxCus);

    cus = new int[randCus];
    cash = new int[numCash];

    for(int i=0; i<numCash; i++)
    {
        cash[i] = 0;
    }
    for (int i=0; i<randCus; i++)
    {
        if(i==0) cus[i] = rand() % (240/randCus);
        else cus[i] = cus[i-1]+rand() % (240/randCus);
    }

 cout<<"random cus: "<<randCus<<endl;

//method
    int wait = 0;
    for(int i=0; i<randCus; i++)
    {
        int servTime = random(minServ, maxServ);
        int j=fastestCashier(cash, numCash);
        if(cash[j] + servTime > 240)
            break;
        if(cash[j] >= cus[i])
        {
            wait += cash[j] - cus[i];
        }
        cout<<"Customer"<<i+1
                <<"\t- Arrived:"<<cus[i]
                //<<"\t- Cashier:"<<cash[j]
                <<"\t- Service time:"<<servTime
                <<"\t- Wait time:"<<(cash[j]>=cus[i]?cash[j]-cus[i]:0)
                <<"\t- Cashier:"<<j+1
                <<"\t- Left:"<<(cash[j]>=cus[i]?cash[j]+servTime:cus[i]+servTime)
                <<endl;
        cash[j]>=cus[i]?cash[j]+=servTime:cash[j] = cus[i]+servTime;
    }
    cout<<"Average wait time: "<<(float)wait/randCus;
}


